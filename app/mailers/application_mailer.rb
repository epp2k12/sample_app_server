class ApplicationMailer < ActionMailer::Base
  default from: "erwinp@casinoespanol.ph"
  layout 'mailer'
end
