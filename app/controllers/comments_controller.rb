class CommentsController < ApplicationController
  def new
  	@comment = Comment.new
  	# store_forwarding_url
  end


  def create

  	f_url = params[:comment][:f_url]
	# get the micropost for the comment from the parameters and crate and instance
        @micropost = Micropost.find_by(id: params[:comment][:micropost_id])   
	@micropost.touch  
	@comment = Comment.new(comment_params)
  
  	if @comment.save 
  		# redirect_to f_url
  		respond_to do |format|
		 format.js
  		 format.html
		end
  	else
		respond_to do |format|
		 format.js {j(render "error_comment")}
		 format.html
		end
  		#flash[:danger] =  "Blanko ang Komentaryo"
  		#redirect_to f_url 
  	end 
  	
  end

  def destroy

   @comment = Comment.find_by(id: params[:id])
   @micropost = Micropost.find_by(id: @comment.micropost_id)
   @comment.destroy
  
   respond_to do |format|
    format.js
    format.html
   end 

   # redirect_to request.referrer || root_url
  end


private

	def comment_params
		params.require(:comment).permit(:user_id, :micropost_id , :comment)
	end

end 
