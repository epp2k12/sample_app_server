class StaticPagesController < ApplicationController

  def home
   
   if logged_in? 
    @micropost = current_user.microposts.build  
    # @feed_items = current_user.feed.paginate(page: params[:page])  
    @microposts = current_user.feed.paginate(page: params[:page], :per_page => 30 ) 
    @comment = Comment.new 
   end
 end

  def help
  end

  def about
  end

  def contact
   @message = Message.new  
  end

  def contact_create
   @message = Message.create(message_params)

   if @message.valid?
    UserMailer.new_message(@message).deliver
    flash[:info] = "Your message has been sent."
    redirect_to contact_path 
  else
    flash.now[:danger] = "An error occured while delivering this message."
    render :contact
   end
	
  end

private

  def message_params
   params.require(:message).permit(:name, :email, :content) 
  end


end
