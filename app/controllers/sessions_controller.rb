class SessionsController < ApplicationController
  def new
  end

  def create
   user = User.find_by(email: params[:session][:email].downcase)
   if user && user.authenticate(params[:session][:password])
    # Log the user in and redirect to the user's show page
    if user.activated? 
     log_in user
     params[:session][:remember_me] == '1' ? remember(user) : forget(user) 
     respond_to do |format| 
      # format.js {  redirect_back_or user_url(user) }
      # format.js { render inline: "window.location.href = '#{user_url(user)}' "}
      format.js { render inline: "window.location.href = '#{root_url}' "}
      format.html
     end
    else
     message = "Account not activated. "
     message += "Check your email for the activation link."
     flash[:danger] = message
    respond_to do |format|
     format.html
     format.js { render inline: "window.location.href = '#{root_url}'" }
    end
    end
    # redirect_to user  ==> original code
   else
    # Create an error messagei
    # user flash.now instead of flash only
    # since render is not considered a request
    # thus the messages will persist on another request
    # flash.now[:danger] = 'Invalid email/password combiination' # not quite right!
    @err_login = "Invalid email/password combination"
    respond_to do |format|
     format.js { render 'new' }
     format.html {  render 'new' }
    end
   end
  end

  def destroy
   log_out if logged_in?
   redirect_to root_url
  end

  def submit

  end

end
