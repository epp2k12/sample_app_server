module SessionsHelper

 #Logs in the given user
 def log_in(user)
  session[:user_id]=user.id
 end
 
 def remember(user)
  user.remember
  cookies.permanent.signed[:user_id] = user.id
  cookies.permanent[:remember_token] = user.remember_token 
 end


# Returns the current logged-in user (if any) 
# This function is already replaced and not used
 def current_user_orig 
  if @current_user.nil? 
   @current_user = User.find_by(id: session[:user_id]) 
  else
   @current_user
  end 
  # or you can use the trinary way
  # @current_user ||= User.find_by(id: session[:user_id]) 
 end

# Returns the user corresponding to the remember token cookie.
 def current_user
  if (user_id = session[:user_id])
   @current_user ||= User.find_by(id: user_id)
  elsif (user_id = cookies.signed[:user_id])
   user = User.find_by(id: user_id) 
   if user && user.authenticated?(:remember, cookies[:remember_token]) 
    log_in user
    @current_user = user
   end
  end
 end


 def logged_in?
  !current_user.nil?
 end 

# forgets a persistent user 
 def forget(user)
  user.forget # assigns nil to remember_digest in database 
  cookies.delete(:user_id)
  cookies.delete(:remember_token) 
 end

# logs out the current user 
 def log_out
  forget(current_user) 
  session.delete(:user_id) 
  @current_user = nil
 end

# returns true if the given user is the current uesr.
 def current_user?(user)
  user == current_user 
 end

# Redirects to stored location (or to the default).
 def redirect_back_or(default)

  redirect_to(session[:forwarding_url] || default) 
  session.delete(:forwarding_url) 

 end

# stores the URL trying to be accessed.
 def store_location
  session[:forwarding_url] = request.url if request.get? 
 end 



end
