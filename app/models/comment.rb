class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :micropost
  default_scope -> { order(created_at: :desc) }
  validates :comment, presence: true, length: { maximum: 140 }

end
